from engine import generator

gen = generator()
gen.parse_yaml('schema.yaml')
gen.generate()
gen.build_models()
