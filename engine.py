class generator(object):
	def __init__(self):
		self.tables = set()
		self.trigger_functions = set()
		self.triggers = set()

	def parse_yaml(self, file_name):
		from yaml import load
		with open(file_name, 'r') as yaml_source:
			self.dump = load(yaml_source.read())

	def generate(self):
		trig_func_out = 'CREATE OR REPLACE FUNCTION update_table()\n'
		trig_func_out += '\tRETURNS TRIGGER AS $$\n'
		trig_func_out += '\tBEGIN\n'
		trig_func_out += '\t\t\n'
		trig_func_out += '\tIF pg_trigger_depth() <> 1 THEN\n'
		trig_func_out += '\t\t\tRETURN NEW;\n'
		trig_func_out += '\t\tEND IF;\n\n'

		for table_name in self.dump:
			name = table_name.lower()
			name_id = str(name)+'_id'

			table_out = 'CREATE TABLE {} (\n'.format(name)
			table_out += '\t{} SERIAL,\n'.format(name_id)
			attributes = self.dump[table_name]['fields']
			for attr in attributes:
				table_out += '\t{}_{} {} NOT NULL,\n'.format(name, attr, attributes[attr])
			table_out += '\t{}_created timestamp DEFAULT localtimestamp NOT NULL,\n'.format(name)
			table_out += '\t{}_updated timestamp,\n'.format(name)
			table_out += '\tPRIMARY KEY({})\n'.format(name_id)
			table_out += ');\n'
			self.tables.add(table_out)

			trig_func_out += '\t\tIF TG_TABLE_NAME = \'{}\' THEN\n'.format(name)
			trig_func_out += '\t\t\tUPDATE {0} SET {0}_updated = localtimestamp\n'.format(name)
			trig_func_out += '\t\t\t\tWHERE NEW.{0}_created = {0}_created;\n'.format(name)
			trig_func_out += '\t\t\tRETURN NEW;\n'
			trig_func_out += '\t\tEND IF;\n'

			tirgger_out = 'CREATE TRIGGER check_update_{}\n'.format(name)
			tirgger_out += '\tAFTER UPDATE ON {}\n'.format(name)
			tirgger_out += '\tFOR EACH ROW\n'
			tirgger_out += '\tEXECUTE PROCEDURE update_table();\n\n'
			self.triggers.add(tirgger_out)

		trig_func_out += '\tEND;\n'
		trig_func_out += '\t$$ LANGUAGE \'plpgsql\';\n\n'
		self.trigger_functions.add(trig_func_out)

	def build_models(self):
		with open('tables.sql', 'w') as out:
			out.write('\n'.join(self.tables))
		with open('trigger_functions.sql', 'w') as out:
			out.write('\n'.join(self.trigger_functions))
		with open('triggers.sql', 'w') as out:
			out.write('\n'.join(self.triggers))
